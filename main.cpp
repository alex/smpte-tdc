#include "ClockTextMFD.h"
#include "ClockTexture.h"
#include "CourierFont.h"

extern "C"
{
#include "smptetdc.h"
}

#include <SFML/Graphics.hpp>

#include <atomic>
#include <iostream>

static bool vsync_enabled;
static unsigned int fps;

std::atomic<bool> running;

inline void reset_clock_time()
{
    clock_h = clock_m = clock_s = clock_ss = clock_ticks = 0;
}

inline void reset_alarm_time()
{
    alarm_h = alarm_m = alarm_s = alarm_ss = alarm_ticks = 0;
}

inline void reset_clock_configuration()
{
    clcfg_run_stop = CLCFG_STOP;
    clcfg_up_down = CLCFG_COUNT_UP;
    clcfg_max_hours = CLCFG_MAX_24;
    clcfg_fps = CLCFG_FPS_24;
    clcfg_alarm = CLCFG_ALARM_OFF;
}

inline void refresh_ui_mfd(sf::Sprite* const sprite)
{
    sprite[0].setTextureRect(sf::IntRect(0, ((clcfg_run_stop == CLCFG_RUN) ? 0 : 24), 99, 24));
    sprite[1].setTextureRect(sf::IntRect(((clcfg_up_down == CLCFG_COUNT_UP) ? 0 : 99), 7 * 24, 99, 24));

    switch (clcfg_max_hours) {
    case CLCFG_MAX_12:
        sprite[2].setTextureRect(sf::IntRect(99, 0 * 24, 99, 24));
        break;
    case CLCFG_MAX_24:
        sprite[2].setTextureRect(sf::IntRect(99, 1 * 24, 99, 24));
        break;
    case CLCFG_MAX_100:
        sprite[2].setTextureRect(sf::IntRect(99, 6 * 24, 99, 24));
        break;
    default:
        break;
    }

    switch (clcfg_fps) {
    case CLCFG_FPS_12:
        sprite[3].setTextureRect(sf::IntRect(99, 0 * 24, 99, 24));
        break;
    case CLCFG_FPS_24:
        sprite[3].setTextureRect(sf::IntRect(99, 1 * 24, 99, 24));
        break;
    case CLCFG_FPS_25:
        sprite[3].setTextureRect(sf::IntRect(99, 2 * 24, 99, 24));
        break;
    case CLCFG_FPS_30:
        sprite[3].setTextureRect(sf::IntRect(99, 3 * 24, 99, 24));
        break;
    case CLCFG_FPS_50:
        sprite[3].setTextureRect(sf::IntRect(99, 4 * 24, 99, 24));
        break;
    case CLCFG_FPS_60:
        sprite[3].setTextureRect(sf::IntRect(99, 5 * 24, 99, 24));
        break;
    case CLCFG_FPS_100:
        sprite[3].setTextureRect(sf::IntRect(99, 6 * 24, 99, 24));
        break;
    default:
        break;
    }

    switch (clcfg_alarm)
    {
    case CLCFG_ALARM_OFF:
        sprite[4].setTextureRect(sf::IntRect(0, 3 * 24, 99, 24));
        break;
    case CLCFG_ALARM_ON:
        sprite[4].setTextureRect(sf::IntRect(0, 2 * 24, 99, 24));
        break;
    case CLCFG_ALARM_ALERT:
        sprite[4].setTextureRect(sf::IntRect(0, 4 * 24, 99, 24));
        break;
    default:
        break;
    }

    sprite[5].setTextureRect(sf::IntRect(0, 5 * 24, 99, 24));
    sprite[6].setTextureRect(sf::IntRect(1, 9 * 24, 99, 24));
    sprite[7].setTextureRect(sf::IntRect(0, 9 * 24, 99, 24));
}

/**
 * Emulates the SysTick timer set to 600 Hz. On a real microcontroller, the timer
 * would be configured by calling SysTick_Config(SystemCoreClock / 600).
 */
void virtual_hwtimer()
{
    sf::Clock clock;
    sf::Time last_time = clock.restart();
    sf::Time delta_time = sf::seconds(1.f / 600); // 600 ticks per second
    while (running) {
        last_time += clock.restart();
        if (last_time > delta_time) {
            SysTick_Handler();
             last_time -= delta_time;
        }
        sf::sleep(sf::milliseconds(1));
    }
}

int main()
{
    std::cout << "Loading fonts." << std::endl;
    sf::Font cour, courbd, courbi, couri;
    bool fontLoadError = false;
    if (!cour.loadFromMemory(cour_ttf, cour_ttf_len)) std::cerr << "ERROR: failed to load font cour" << std::endl, fontLoadError = true;
    if (!courbd.loadFromMemory(courbd_ttf, courbd_ttf_len)) std::cerr << "ERROR: failed to load font courbd" << std::endl, fontLoadError = true;
    if (!courbi.loadFromMemory(courbi_ttf, courbi_ttf_len)) std::cerr << "ERROR: failed to load font courbi" << std::endl, fontLoadError = true;
    if (!couri.loadFromMemory(couri_ttf, couri_ttf_len)) std::cerr << "ERROR: failed to load font couri" << std::endl, fontLoadError = true;
    if (fontLoadError)
        return -1;

    std::cout << "Loading textures." << std::endl;
    sf::Texture clock_font_texture, clock_text_mfd_texture;
    bool texLoadError = false;
    if (!clock_font_texture.loadFromMemory(clock_texture, clock_texture_len)) std::cerr << "ERROR: failed to load clock textures" << std::endl, texLoadError = true;
    if (!clock_text_mfd_texture.loadFromMemory(clock_text_mfd_tex, clock_text_mfd_tex_len)) std::cerr << "ERROR: failed to load MFD textures" << std::endl, texLoadError = true;
    if (texLoadError)
        return -2;
    clock_font_texture.setRepeated(false);
    clock_font_texture.setSmooth(false);

    std::cout << "Creating sprites." << std::endl;
    sf::VertexArray virtual_screen_background(sf::Quads, 4);
    for (size_t i = 0; i < 4; ++i) virtual_screen_background[i].color = sf::Color::Black;
    virtual_screen_background[0].position = sf::Vector2f(50.f, 50.f);
    virtual_screen_background[1].position = sf::Vector2f(50.f, 250.f);
    virtual_screen_background[2].position = sf::Vector2f(850.f, 250.f);
    virtual_screen_background[3].position = sf::Vector2f(850.f, 50.f);
    sf::VertexArray virtual_buttons(sf::Quads, 32);
    for (size_t i = 0; i < 32; ++i) {
        virtual_buttons[i].color = sf::Color(83, 83, 83);
        virtual_buttons[i].position = sf::Vector2f((((i / 2) % 2) ? 19.f : -20.f) + ((i / 4 + 1) * 100.f), ((i + 1) % 4 < 2) ? 306.f : 280.f);
    }
    sf::VertexArray virtual_button_labels(sf::Triangles, 24);
    for (size_t i = 0; i < 24; ++i) {
        virtual_button_labels[i].color = sf::Color(239, 239, 239);
        virtual_button_labels[i].position = sf::Vector2f((((i + 1) % 3) * 9.5f - 10.f) + ((i / 3 + 1) * 100.f), (i % 3) ? 298.f : 288.f);
    }
    sf::VertexArray virtual_button_lines(sf::Quads, 32);
    for (size_t i = 0; i < 32; ++i) {
        virtual_button_lines[i].color = sf::Color(15, 15, 15);
        virtual_button_lines[i].position = sf::Vector2f((((i / 2) % 2) ? 1.f : -2.f) + ((i / 4 + 1) * 100.f), ((i + 1) % 4 < 2) ? 280.f : 250.f);
    }
    sf::VertexArray ui_lines(sf::Lines, 16);
    for (size_t i = 0; i < 16; ++i) ui_lines[i].color = sf::Color::White;
    ui_lines[0].position = sf::Vector2f(50.f, 225.f);
    ui_lines[1].position = sf::Vector2f(849.f, 225.f);
    for (size_t i = 2; i < 16; ++i) ui_lines[i].position = sf::Vector2f((i / 2) * 100.f + 50.f, (i % 2) ? 225.f : 250.f);
    sf::Sprite ui_mfd_text[8];
    for (size_t i = 0; i < 8; ++i) {
        ui_mfd_text[i].setTexture(clock_text_mfd_texture);
        ui_mfd_text[i].setTextureRect(sf::IntRect(0, i * 24, 99, 24));
        ui_mfd_text[i].setScale(sf::Vector2f(1.f, 1.f));
        ui_mfd_text[i].move(50.f + (100.f * i), 226.f);
    }



    /**********************************************************************************/


    sf::Sprite sprite_digits[8];
    for (size_t i = 0; i < 8; ++i) {
        sprite_digits[i].setTexture(clock_font_texture);
        //sprite_digits[i].setTextureRect(sf::IntRect((i % 5) * 21, (i / 5) * 30, 21, 30));
        sprite_digits[i].setScale(sf::Vector2f(2.f, 2.f));
        sprite_digits[i].move(150.f + (25.f * i * 2) + (i / 2) * 40.f, 110.f);
    }
    sf::Sprite sprite_ampm(clock_font_texture);
    sprite_ampm.setTexture(clock_font_texture);
    sprite_ampm.setTextureRect(sf::IntRect(105, 13, 27, 13));
    sprite_ampm.setScale(sf::Vector2f(2.f, 2.f));
    sprite_ampm.move(700.f, 110.f);
    sf::VertexArray separators(sf::Quads, 24);
    for (size_t i = 0; i < 24; ++i) {
        switch (i % 4) {
        case 0:
            separators[i].position = sf::Vector2f((i / 4) * 20.f, 60.f);
            break;
        case 1:
            separators[i].position = sf::Vector2f((i / 4) * 20.f + 10, 60.f);
            break;
        case 2:
            separators[i].position = sf::Vector2f((i / 4) * 20.f + 10, 70.f);
            break;
        default:
            separators[i].position = sf::Vector2f((i / 4) * 20.f, 70.f);
        }
        separators[i].color = sf::Color(223, 113, 38);
    }
    


    std::cout << "Starting virtual hwtimer." << std::endl;
    running = true;
    sf::Thread virtual_hwtimer_thread(&virtual_hwtimer);
    virtual_hwtimer_thread.launch();

    std::cout << "Creating main window." << std::endl;
    sf::RenderWindow window(sf::VideoMode(900, 350), "SMPTE Timecode Desktop Clock Emulator", sf::Style::Titlebar | sf::Style::Close);

    vsync_enabled = true;
    fps = 30;
    window.setVerticalSyncEnabled(vsync_enabled);
    window.setFramerateLimit(fps);

    reset_clock_configuration();
    reset_clock_time();
    reset_alarm_time();
    displ_h = -1; // this hack ensures that the display is refreshed initially

    sf::Text textFPS[2];
    for (size_t c = 0; c < 2; ++c) {
        textFPS[c].setFont(courbd);
        textFPS[c].setCharacterSize(14);
        textFPS[c].setFillColor(sf::Color(255, 255, 255, 127));
        textFPS[c].setPosition(700.f, 10.f + (c * 15.f));
    }

    sf::Clock clock;
    unsigned int fpsCounter = 0;
    float lastFPSTime = 0.0f;
    float lastTime = 0.0f;
    float currentTime = 0.0f;
    float delta;

    while (running) {
        // check all events in the queue
        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
            case sf::Event::Closed:
                running = false;
                window.close();
                break;
            case sf::Event::MouseButtonPressed:
                printf("[\033[33mMouse   \033[0m]: %d, %d\n", event.mouseButton.x, event.mouseButton.y);
                if (event.mouseButton.y >= 280 && event.mouseButton.y <= 305) {
                    if (event.mouseButton.x >= 80 && event.mouseButton.x <= 118) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 1);
                        clcfg_run_stop = (clcfg_run_stop == CLCFG_STOP) ? CLCFG_RUN : CLCFG_STOP;
                    }
                    if (event.mouseButton.x >= 180 && event.mouseButton.x <= 218) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 2);
                        clcfg_up_down = (clcfg_up_down == CLCFG_COUNT_UP) ? CLCFG_COUNT_DOWN : CLCFG_COUNT_UP;
                    }
                    if (event.mouseButton.x >= 280 && event.mouseButton.x <= 318) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 3);
                        switch (clcfg_max_hours)
                        {
                        case CLCFG_MAX_12:
                            clcfg_max_hours = CLCFG_MAX_24;
                            break;
                        case CLCFG_MAX_24:
                            clcfg_max_hours = CLCFG_MAX_100;
                            break;
                        default: // CLCFG_MAX_100
                            clcfg_max_hours = CLCFG_MAX_12;
                            break;
                        }
                    }
                    if (event.mouseButton.x >= 380 && event.mouseButton.x <= 418) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 4);
                        switch (clcfg_fps)
                        {
                        case CLCFG_FPS_12:
                            clcfg_fps = CLCFG_FPS_24;
                            break;
                        case CLCFG_FPS_24:
                            clcfg_fps = CLCFG_FPS_25;
                            break;
                        case CLCFG_FPS_25:
                            clcfg_fps = CLCFG_FPS_30;
                            break;
                        case CLCFG_FPS_30:
                            clcfg_fps = CLCFG_FPS_50;
                            break;
                        case CLCFG_FPS_50:
                            clcfg_fps = CLCFG_FPS_60;
                            break;
                        case CLCFG_FPS_60:
                            clcfg_fps = CLCFG_FPS_100;
                            break;
                        default: // CLCFG_FPS_100
                            clcfg_fps = CLCFG_FPS_12;
                            break;
                        }
                    }
                    if (event.mouseButton.x >= 480 && event.mouseButton.x <= 518) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 5);
                        switch (clcfg_alarm)
                        {
                        case CLCFG_ALARM_OFF:
                            clcfg_alarm = CLCFG_ALARM_ON;
                            break;
                        case CLCFG_ALARM_ON:
                            clcfg_alarm = CLCFG_ALARM_OFF;
                            break;
                        default: // CLCFG_ALARM_ALERT
                            clcfg_alarm = CLCFG_ALARM_OFF;
                            break;
                        }
                    }
                    if (event.mouseButton.x >= 580 && event.mouseButton.x <= 618) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 6);
                        reset_clock_time();
                    }
                    if (event.mouseButton.x >= 680 && event.mouseButton.x <= 718) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 7);

                    }
                    if (event.mouseButton.x >= 780 && event.mouseButton.x <= 818) {
                        printf("[\033[34mMouse   \033[0m]: MFD button %d\n", 8);

                    }
                }
                break;
            case sf::Event::KeyPressed:
                printf("[\033[33mKeyPress\033[0m]: %u\n", (unsigned int)event.key.code);
                switch (event.key.code) {
                case sf::Keyboard::Escape:
                case sf::Keyboard::Q:
                    running = false;
                    window.close();
                    break;
                case sf::Keyboard::V:
                    window.setVerticalSyncEnabled(vsync_enabled = !vsync_enabled);
                    break;
                case sf::Keyboard::F:
                    window.setFramerateLimit(fps = (fps == 30) ? 60 : 30);
                    break;
                default:
                    break;
                    //currentSim->HandleKey(event.key);
                }
                break;
            default:
                break;
            }
        }

        main_loop();

        if (update_display_flag)
        {
            sprite_digits[0].setTextureRect(sf::IntRect(((displ_h / 10) % 5) * 21, ((displ_h / 10) / 5) * 30, 21, 30));
            sprite_digits[1].setTextureRect(sf::IntRect(((displ_h % 10) % 5) * 21, ((displ_h % 10) / 5) * 30, 21, 30));
            sprite_digits[2].setTextureRect(sf::IntRect(((displ_m / 10) % 5) * 21, ((displ_m / 10) / 5) * 30, 21, 30));
            sprite_digits[3].setTextureRect(sf::IntRect(((displ_m % 10) % 5) * 21, ((displ_m % 10) / 5) * 30, 21, 30));
            sprite_digits[4].setTextureRect(sf::IntRect(((displ_s / 10) % 5) * 21, ((displ_s / 10) / 5) * 30, 21, 30));
            sprite_digits[5].setTextureRect(sf::IntRect(((displ_s % 10) % 5) * 21, ((displ_s % 10) / 5) * 30, 21, 30));
            sprite_digits[6].setTextureRect(sf::IntRect(((displ_ss / 10) % 5) * 21, ((displ_ss / 10) / 5) * 30, 21, 30));
            sprite_digits[7].setTextureRect(sf::IntRect(((displ_ss % 10) % 5) * 21, ((displ_ss % 10) / 5) * 30, 21, 30));

            if (clcfg_max_hours == CLCFG_MAX_12)
                sprite_ampm.setTextureRect(sf::IntRect(105, (clock_h / 12) ? 13 : 0, 27, 13));
        }

        currentTime = clock.getElapsedTime().asSeconds();
        delta = currentTime - lastTime;
        lastTime = currentTime;
        textFPS[0].setString(std::string("d: ") + std::to_string(delta));

        int update = 1;
        if (update) {
            refresh_ui_mfd(ui_mfd_text);

            window.clear(sf::Color(47, 47, 47));
            window.draw(virtual_screen_background);

            for (size_t i = 0; i < 8; ++i) window.draw(sprite_digits[i]);

            if (clcfg_max_hours == CLCFG_MAX_12)
                window.draw(sprite_ampm);
            //window.draw(separators);
            window.draw(virtual_buttons);
            window.draw(virtual_button_labels);
            window.draw(virtual_button_lines);
            window.draw(ui_lines);

            for (size_t i = 0; i < 8; ++i) window.draw(ui_mfd_text[i]);

            // draw fps counter overlay and display final image
            window.draw(textFPS[0]);
            window.draw(textFPS[1]);
            window.display();
        }

        ++fpsCounter;
        if (currentTime - lastFPSTime >= 1.0f) {
            textFPS[1].setString(std::string("FPS: ") + std::to_string(fpsCounter));
            fpsCounter = 0;
            lastFPSTime = currentTime;
        }
    }

    virtual_hwtimer_thread.wait();
    return 0;
}