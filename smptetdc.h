#pragma once

#pragma warning(disable : 26812)

enum clcfg_run_stop_t { CLCFG_RUN, CLCFG_STOP };
enum clcfg_up_down_t { CLCFG_COUNT_UP, CLCFG_COUNT_DOWN };
enum clcfg_max_hours_t { CLCFG_MAX_12 = 12, CLCFG_MAX_24 = 24, CLCFG_MAX_100 = 100 };
enum clcfg_fps_t { CLCFG_FPS_12 = 50, CLCFG_FPS_24 = 25, CLCFG_FPS_25 = 24, CLCFG_FPS_30 = 20, CLCFG_FPS_50 = 12, CLCFG_FPS_60 = 10, CLCFG_FPS_100 = 6 };
enum clcfg_alarm_t { CLCFG_ALARM_OFF, CLCFG_ALARM_ON, CLCFG_ALARM_ALERT };

extern enum clcfg_run_stop_t clcfg_run_stop;
extern enum clcfg_up_down_t clcfg_up_down;
extern enum clcfg_max_hours_t clcfg_max_hours;
extern enum clcfg_fps_t clcfg_fps;
extern enum clcfg_alarm_t clcfg_alarm;

extern int clock_h, clock_m, clock_s, clock_ss, clock_ticks;
extern int alarm_h, alarm_m, alarm_s, alarm_ss, alarm_ticks;
extern int displ_h, displ_m, displ_s, displ_ss;
extern int tmpui_h, tmpui_m, tmpui_s, tmpui_ss;

extern int update_display_flag;

void main_loop(void);

void SysTick_Handler(void);