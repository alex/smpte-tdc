#include "smptetdc.h"

#include <stdio.h>


enum clcfg_run_stop_t clcfg_run_stop;
enum clcfg_up_down_t clcfg_up_down;
enum clcfg_max_hours_t clcfg_max_hours;
enum clcfg_fps_t clcfg_fps;
enum clcfg_alarm_t clcfg_alarm;

int clock_h, clock_m, clock_s, clock_ss, clock_ticks;
int alarm_h, alarm_m, alarm_s, alarm_ss, alarm_ticks;
int displ_h, displ_m, displ_s, displ_ss;
int tmpui_h, tmpui_m, tmpui_s, tmpui_ss;

int update_display_flag;

void SysTick_Handler(void)
{
	if (clcfg_run_stop == CLCFG_RUN) {
		clock_ticks = (clcfg_up_down == CLCFG_COUNT_UP) ? clock_ticks + 1 : clock_ticks - 1;
	}
}

/*
void TIMER0_IRQHandler(void)
{
	if (LPC_TMR16B0->IR & (1 << 4))            //capture interrupt?
	{
		LPC_TMR16B0->TC = 0;                    //reset timer
		count++;
		LPC_TMR16B0->IR |= (1 << 4);            //reset capture interrupt
	}                                         //end capture interrupt
}

}*/

/*
void timer_overflow_ISR()
{
	++clock_ticks;
	if (clock_ticks > 599) ++clock_s, clock_ticks = 0;
	if (clock_s > 59) ++clock_m, clock_s = 0;
	if (clock_m > 59) ++clock_h, clock_m = 0;
	if (clock_h > clcfg_max_hours) clock_h = 0;
	clock_ss = clock_ticks / clcfg_fps;

	if (!clock_ticks)
		std::cout << "[CLOCK]" << std::to_string(clock_h) << ':' << std::to_string(clock_m) << ':' << std::to_string(clock_s) << ':' << std::to_string(clock_ss) << std::endl, main_loop();
}*/

void main_loop(void)
{
	if (clcfg_up_down == CLCFG_COUNT_UP) {
		if (clock_ticks >= 600) {
			clock_ticks -= 600; // DANGER: non-atomic operation may fail
			clock_s += 1;
		}
		clock_ss = clock_ticks / clcfg_fps;
		if (clock_s >= 60) {
			clock_m += 1;
			clock_s -= 60;
		}
		if (clock_m >= 60) {
			clock_h += 1;
			clock_m -= 60;
		}
		if (clcfg_max_hours == CLCFG_MAX_100) {
			if (clock_h >= 100)
				clock_h -= 100;
		} else if (clock_h >= 24) {
			clock_h -= 24;
		}
	} else {
		if (clock_ticks < 0) {
			clock_ticks += 600; // DANGER: non-atomic operation may fail
			clock_s -= 1;
		}
		clock_ss = clock_ticks / clcfg_fps;
		if (clock_s < 0) {
			clock_m -= 1;
			clock_s += 60;
		}
		if (clock_m < 0) {
			clock_h -= 1;
			clock_m += 60;
		}
		if (clock_h < 0) {
			clock_h = (clcfg_max_hours == CLCFG_MAX_100) ? 99 : 23;
		}
	}

	// if anything displayable has changed, update the display
	update_display_flag = ((displ_h != clock_h) || (displ_m != clock_m) || (displ_s != clock_s) || (displ_ss != clock_ss));
	if (1) {
		if (clcfg_max_hours == CLCFG_MAX_12) {
			displ_h = clock_h % 12;
			if (!displ_h) displ_h += 12;
		} else {
			displ_h = clock_h;
		}
		displ_m = clock_m;
		displ_s = clock_s;
		displ_ss = clock_ss;
	}
}
